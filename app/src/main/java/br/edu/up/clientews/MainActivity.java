package br.edu.up.clientews;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  public void onClickListar(View v){

    String url = "http://ws-briatore.rhcloud.com/ws/pessoa/listar";

    StringRequest request = new StringRequest(Request.Method.GET, url,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {
            Log.d("STRING REQUEST" , response);
          }
        },
        new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            //Caso dê algum erro fazer o tratamento...
            Log.d("STRING REQUEST", "Erro na requisição!");
          }
        }
    );

    RequestQueue queue = Volley.newRequestQueue(this);
    queue.add(request);

  }

  public void onClickSalvar(View v){

    String url = "http://ws-briatore.rhcloud.com/ws/pessoa/salvar";

    /*
    <cpf>333.928.048-78</cpf>
    <dataNascimento>1995-01-23T00:00:00-05:00</dataNascimento>
    <estadoCivil>Solteiro</estadoCivil>
    <id>4</id>
    <local>Curitiba</local>
    <nome>André</nome>
    <rg>43.559.201-4</rg>
    <sexo>77</sexo>
     */


    Map<String, String> map = new HashMap<>();
    map.put("cpf","123456");
    map.put("dataNascimento", "1995-01-23T00:00:00-05:00");
    map.put("estadoCivil", "Casado");
    map.put("nome", "TESTE ECO");
    map.put("rg","123456");
    map.put("local", "Curitiba");
    map.put("sexo","M");

    JSONObject jsonObject = new JSONObject(map);
    JSONArray jsonArray = new JSONArray();
    jsonArray.put(jsonObject);

    JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url,
    //JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url,
        jsonArray,
        new Response.Listener<JSONArray>() {
          @Override
          public void onResponse(JSONArray response) {
            Log.d("JSON REQUEST", "Requisição efetuada com sucesso!");
          }
        },
        new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            Log.d("JSON REQUEST", "Erro na requisição!");
            Log.d("JSON REQUEST", error.getLocalizedMessage());
          }
        }
    ){
      protected Response<JSONArray> parseNetworkResponse(NetworkResponse nr) {
        try {
          String header = HttpHeaderParser.parseCharset(nr.headers, "utf-8");
          String js = new String(nr.data, header);
          JSONArray obj = null;
          if (js != null && js.length() > 0) {
            obj = new JSONArray(js);
          }
          return Response.success(obj, HttpHeaderParser.parseCacheHeaders(nr));
        } catch (Exception e) {
          return Response.error(new ParseError(e));
        }
      }
    };

    RequestQueue queue = Volley.newRequestQueue(this);
    queue.add(request);

  }

}
